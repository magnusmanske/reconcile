<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); # |E_ALL
ini_set('display_errors', 'On');
set_time_limit ( 60 * 10 ) ; // Seconds
require_once ( 'php/common.php' ) ;
require_once ( '/data/project/catscan2/public_html/omniscan.inc' ) ;

$lang_prefs = array ( 'en' , 'de' , 'es' , 'fr' , 'it' , 'nl' , 'zh' ) ;

$lang2rank = array() ;
foreach ( $lang_prefs AS $k => $v ) $lang2rank[$v] = $k ;

// Lower is better
function getLanguageRank ( $lang ) {
	global $lang2rank ;
	if ( isset ( $lang2rank[$lang] ) ) return $lang2rank[$lang] ;
	return count($lang2rank) + 1 ; // Whateva
}

function runQuery ( $j ) {
	global $db , $wdq_internal_url ;
	$ret = (object) array ( 'result' => array() ) ;
	
	if ( $j == null ) return $ret ;
	
	// Wikidata text search
	$url = 'https://www.wikidata.org/w/api.php?action=query&list=search&srnamespace=0&srlimit=50&srprop=&format=json&srsearch=' . urlencode ( $j->query ) ;
	$search = json_decode ( file_get_contents ( $url ) ) ;
	if ( $search == null ) return $ret ; // Something's wrong
	if ( !isset ( $search->query ) ) return $ret ; // Something's wrong
	if ( !isset ( $search->query->search ) ) return $ret ; // Something's wrong
	
	if ( isset($search->query->searchinfo) and isset($search->query->searchinfo->totalhits) ) {
		$ret->total_search_results = $search->query->searchinfo->totalhits ;
	}
	
	if ( count ( $search->query->search ) == 0 ) return $ret ; // No results
	
	
	// Initialize results
	foreach ( $search->query->search AS $k => $v ) {
		$r = (object) array() ;
		$r->id = $v->title ;
		$r->score = 1/($k+2) ; // Not sure how to set this...
		$r->match = false ; // Will be set later for single, perfect name matches
		$r->type = array() ;
		$ret->result[] = $r ;
	}
	
	$need_types = true ;
	
	// Types
	if ( isset ( $j->type ) and count ( $ret->result ) > 0 ) {
		$need_types = false ;
		$types = $j->type ;
		if ( is_string($types) ) $types = array ( $types ) ;
		$type_strict = 'any' ; // Default
		if ( isset($j->type_strict) ) $type_strict = $j->type_strict ;
		
		
		$sparql = "PREFIX wdt: <http://www.wikidata.org/prop/direct/>\n" ;
		$sparql .= "PREFIX wd: <http://www.wikidata.org/entity/>\n" ;
		$sparql .= "PREFIX wikibase: <http://wikiba.se/ontology#>\n" ;
		$sparql .= "SELECT ?s ?o0 ?sLabel ?o0Label WHERE {\n" ;
		$sparql .= "  VALUES (?s) { " ;
		foreach ( $ret->result AS $k => $v ) $sparql .= "(wd:{$v->id}) " ;
		$sparql .= "}.\n" ;
		if ( $type_strict ) {
			foreach ( $types AS $nt => $t ) $sparql .= "  ?o$nt (wdt:P279)* wd:$t .\n" ;
			foreach ( $types AS $nt => $t ) $sparql .= "?s wdt:P31 ?o$nt .\n" ;
		} else {
			$sparql .= "  VALUES (?types) { " ; //(wd:Q515) (wd:Q1066984) } .
			foreach ( $types AS $nt => $t ) $sparql .= "(wd:$t) " ;
			$sparql .= "} .\n" ;
			$sparql .= "  ?o0 (wdt:P279)* ?types .\n" ;
			$sparql .= "?s wdt:P31 ?o0 .\n" ;
		}
		$sparql .= "  SERVICE wikibase:label { bd:serviceParam wikibase:language \"en\" } }" ;
		
		$url = "https://query.wikidata.org/bigdata/namespace/wdq/sparql?format=json&query=" . urlencode($sparql) ;
		$j2 = json_decode ( file_get_contents ( $url ) ) ;
		
		$survivors = array() ;
		foreach ( $j2->results->bindings AS $row ) {
			$q = preg_replace ( '/^.+\//' , '' , $row->{'s'}->{'value'} ) ;
			if ( !isset($survivors[$q]) ) $survivors[$q] = array() ;
			
			$q0 = preg_replace ( '/^.+\//' , '' , $row->{'o0'}->{'value'} ) ;
			$l0 = $row->{'o0Label'}->{'value'} ;
			$survivors[$q][$q0] = $l0 ;
		}
		foreach ( $ret->result AS $k => $v ) {
			if ( !isset($survivors[$v->id]) ) {
				unset ( $ret->result[$k] ) ;
			} else {
				foreach ( $survivors[$v->id] AS $k0 => $v0 ) {
					$ret->result[$k]->type[] = array ( 'id' => $k0 , 'name' => $v0 ) ;
				}
			}
		}


		if ( count ( $ret->result ) == 0 ) return $ret ;
	}
	
	# Enforce result array; could do (array) but...
	$r2 = array() ;
	foreach ( $ret->result AS $k => $v ) $r2[] = $v ;
	$ret->result = $r2 ;

	
	// Limit
	$limit = 3 ;
	if ( isset($j->limit) ) {
		$j->limit = round ( $j->limit * 1 ) ;
		if ( $j->limit >= 1 and $j->limit < $limit ) $limit = $j->limit ;
	}
	while ( count($ret->result) > $limit ) array_pop ( $ret->result ) ;

		
	// Get labels
	$qs = array() ;
	foreach ( $ret->result AS $r ) {
		$qs[] = preg_replace ( '/\D/' , '' , $r->id ) ;
	}
	$labels = array() ;
	$sql = "SELECT * FROM wb_terms WHERE term_entity_id IN (" . implode(',',$qs) . ") AND term_entity_type='item' AND term_type='label'" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n\n$sql\n");
	while($o = $result->fetch_object()){
		if ( isset($labels[$o->term_entity_id]) ) {
			$new_rank = getLanguageRank($o->term_language) ;
			if ( $new_rank < $labels[$o->term_entity_id][1] ) {
				$labels[$o->term_entity_id] = array ( $o->term_text , getLanguageRank($o->term_language) ) ;
			}
		} else {
			$labels[$o->term_entity_id] = array ( $o->term_text , getLanguageRank($o->term_language) ) ;
		}
	}
	
	// Get type(s) per item
	if ( $need_types ) {
		$types = array() ;
		$query = "items[" . implode ( ',' , $qs ) . "]" ;
		foreach ( array ( 31 , 279 ) as $prop ) {
			$url = "$wdq_internal_url?q=" . urlencode($query) . "&props=$prop" ;
			$j2 = json_decode ( file_get_contents ( $url ) ) ;
			if ( $j2 ) {
				foreach ( $j2->props->$prop AS $v ) {
					if ( $v[1] == 'item' ) $types[''.$v[0]][] = 'Q'.$v[2] ;
				}
			}
		}
		foreach ( $ret->result AS $k => $v ) {
			$q = preg_replace ( '/\D/' , '' , $v->id ) ;
			if ( !isset($types[$q]) ) continue ;
			$v->type = $types[$q] ;
		}
	}
	
	// Scoring and "match"
	$best = array() ;
	foreach ( $ret->result AS $k => $v ) {
		$q = preg_replace ( '/\D/' , '' , $v->id ) ;
		if ( !isset($labels[$q]) ) continue ;
		$v->name = $labels[$q][0] ;
		if ( strtolower($v->name) == strtolower($j->query) ) {
			$v->score *= 2 ;
			$best[] = $k ;
		}
	}
	
	// If there is a single, perfect name match to the query, this becomes the "match"
	if ( count($best) == 1 ) {
		$ret->result[$best[0]]->match = true ;
	}
	
	return $ret ;
}

$query = get_request ( 'query' , '' ) ;
$queries = get_request ( 'queries' , '' ) ;
$callback = get_request ( 'callback' , '' ) ;

if ( $query == '' and $queries == '' and $callback == '' ) {
	print get_common_header ( '' , 'Wikidata Reconcile' ) ;
	
	print "<div class='lead'>
	This tool offers a <a href='https://github.com/OpenRefine/OpenRefine/wiki/Reconcilable-Data-Sources'>reconcile API</a> for Wikidata.
	</div>" ;
	
	print "<div>
	<form method='get' action='?' class='form form-inline'>
	<input type='text' name='query' value='' placeholder='e.g. Berlin' style='width:100%' /><br/>
	<input type='submit' name='' value='Run single query' class='btn btn-primary' />
	</form>
	</div>" ;

	print "<div>
	<form method='get' action='?' class='form form-inline'>
	<textarea name='queries' placeholder='JSON for multiple queries' rows='5' style='width:100%'></textarea>
	<input type='submit' name='' value='Run multiple queries' class='btn btn-primary' />
	</form>
	</div>" ;
	
	print "<div><hr/>
	<h2>Examples</h2>
	<ul>
	<li><a href='./?query={%22query%22%3A%22cambridge%22%2C%22type%22%3A%22Q3918%22}'>" . 'query={"query":"cambridge","type":"Q3918"}</a>' . "
	Searches for 'Cambridge' with type 'University' (<a href='//www.wikidata.org/wiki/Q3918'>Q3918</a>).
	<br/>Results will be restricted to instances (<a href='//www.wikidata.org/wiki/Property:P31'>P31</a>) or subclasses (<a href='//www.wikidata.org/wiki/Property:P279'>P279</a>) of 'University', or any of its subclasses.</li>
	</ul>
	</div>" ;
	
	print "<div style='font-size:9pt'><hr/>
	<p>This tool is under construction. API implementation is incomplete. Things may change or stop working at any time.</p>
	<p>Supported:
	<ul>
	<li><a href='?callback=json'>Service Metadata</a> (partially)</li>
	<li>Simple single query and multiple queries</li>
	<li>'query' parameter</li>
	<li>'limit' parameter</li>
	<li>'type' parameter</li>
	<li>'type strict' parameter (except 'should')</li>
	</ul>
	</p>
	<p>Not yet supported:
	<ul>
	<li>Preview API</li>
	<li>Query 'properties' parameter</li>
	</ul>
	<i>Note:</i> This tool does run a Wikidata search with max 50 results, then filters it on 'type' if given. Valid results may be skipped due to the Wikidata search API limitation to 50 results per query.
	</p>
	</div>" ;
	
	print get_common_footer() ;
	exit ( 0 ) ;
}

header('Content-type: application/json; charset=UTF-8');
header("Cache-Control: no-cache, must-revalidate");

$db = openDB ( 'wikidata' , 'wikidata' ) ;

$out = (object) array () ;
if ( $query != '' and $query != null ) {
	$j = json_decode ( $query ) ;
	if ( $j == null ) $j = array ( 'query' => $query ) ;
	$out = runQuery ( (object) $j ) ;
}

if ( $queries != '' ) {
	$j = json_decode ( $queries ) ;
	foreach ( $j AS $k => $v ) {
		$out->$k = runQuery ( (object) $v ) ;
	}
}

if ( $query == '' and $queries == '' and $callback != '' ) {
	$out = json_decode ( '{"name":"Wikidata Reconciliation Service","view":{"url":"https://www.wikidata.org/wiki/{{id}}"}}' ) ;
}

//print_r ( $out ) ; // TESTING

if ( isset($_REQUEST['callback']) ) print $_REQUEST['callback'] . "(" ;
print json_encode ( $out ) ;
if ( isset($_REQUEST['callback']) ) print ")" ;


?>
# README #

This implements a (partial) reconcile API against Wikidata.

Live tool is [here](https://tools.wmflabs.org/wikidata-reconcile/).

Requires omniscan.inc from [here](https://bitbucket.org/magnusmanske/catscan2/src/60b2fdddee81396cffc83c2729b9e71454d08f0d/public_html/?at=master).